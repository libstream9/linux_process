#ifndef STREAM9_LINUX_SETSID_HPP
#define STREAM9_LINUX_SETSID_HPP

#include <stream9/safe_integer.hpp>

#include <unistd.h>

namespace stream9::linux {

safe_integer<::pid_t, 0> setsid();

} // namespace stream9::linux

#endif // STREAM9_LINUX_SETSID_HPP

#ifndef STREAM9_LINUX_FORK_HPP
#define STREAM9_LINUX_FORK_HPP

#include <stream9/linux/error.hpp>
#include <stream9/linux/namespace.hpp>
#include <stream9/linux/pid.hpp>

namespace stream9::linux {

pid fork();

} // namespace stream9::linux

#endif // STREAM9_LINUX_FORK_HPP

#ifndef STREAM9_LINUX_PID_HPP
#define STREAM9_LINUX_PID_HPP

#include <stream9/linux/error.hpp>
#include <stream9/linux/namespace.hpp>

#include <sys/types.h>

namespace stream9::linux {

class pid
{
public:
    // essential
    explicit pid(::pid_t id);

    pid(pid const&) = delete;
    pid& operator=(pid const&) = delete;

    pid(pid&&) = default;
    pid& operator=(pid&&) = default;

    ~pid() noexcept;

    // accessor
    operator ::pid_t () const noexcept { return m_id; }

    // modifier
    ::pid_t release() noexcept;

private:
    ::pid_t m_id = -1;
};

} // namespace stream9::linux

#endif // STREAM9_LINUX_PID_HPP

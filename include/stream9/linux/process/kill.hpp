#ifndef STREAM9_LINUX_PROCESS_KILL_HPP
#define STREAM9_LINUX_PROCESS_KILL_HPP

#include <signal.h>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

void kill(::pid_t, int sig);

} // namespace stream9::linux

#endif // STREAM9_LINUX_PROCESS_KILL_HPP

#ifndef STREAM9_LINUX_PROCESS_PIDFD_HPP
#define STREAM9_LINUX_PROCESS_PIDFD_HPP

#include <signal.h>
#include <unistd.h>

#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>

namespace stream9::linux {

class pidfd
{
public:
    explicit pidfd(::pid_t pid, unsigned int flags = 0);

    // accessor
    fd_ref fd() const noexcept { return m_fd; }

    // query
    linux::fd getfd(fd_ref targetfd, unsigned int flags = 0);

    // command
    void send_signal(int sig, ::siginfo_t& info, unsigned int flags = 0);
    void send_signal(int sig, unsigned int flags = 0);

    // conversion
    operator fd_ref () const noexcept { return m_fd; }

    // comparison
    bool operator==(fd_ref const fd) const noexcept { return m_fd == fd; }

private:
    linux::fd m_fd;
};

} // namespace stream9::linux

#endif // STREAM9_LINUX_PROCESS_PIDFD_HPP

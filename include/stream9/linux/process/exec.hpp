#ifndef STREAM9_LINUX_PROCESS_EXEC_HPP
#define STREAM9_LINUX_PROCESS_EXEC_HPP

#include <stream9/args.hpp>
#include <stream9/array_view.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/namespace.hpp>
#include <stream9/outcome.hpp>

namespace stream9::linux {

[[noreturn]] void
exec(cstring_ptr filename,
     args const& argv);

[[noreturn]] void
exec(cstring_ptr filename,
     args const& argv,
     args const& envp);

[[noreturn]] void
execp(cstring_ptr filename,
      args const& argv);

[[noreturn]] void
execp(cstring_ptr filename,
      args const& argv,
      args const& envp);

/*
 * nothrow API
 */
namespace nothrow {

outcome<void, errc>
exec(cstring_ptr filename,
     args const& argv);

outcome<void, errc>
exec(cstring_ptr filename,
     args const& argv,
     args const& envp);

outcome<void, errc>
execp(cstring_ptr filename,
      args const& argv);

outcome<void, errc>
execp(cstring_ptr filename,
      args const& argv,
      args const& envp);

} // namespace nothrow

} // namespace stream9::linux

#endif // STREAM9_LINUX_PROCESS_EXEC_HPP

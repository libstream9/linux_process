#ifndef STREAM9_LINUX_PROCESS_WAIT_HPP
#define STREAM9_LINUX_PROCESS_WAIT_HPP

#include <sys/wait.h>

#include <stream9/linux/error.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::linux {

safe_integer<::pid_t, 0>
    wait(int* wstatus = nullptr);

safe_integer<::pid_t, 0>
    waitpid(::pid_t pid, int* wstatus = nullptr);

safe_integer<::pid_t, 0>
    waitpid(::pid_t pid, int* wstatus, int options);

} // namespace stream9::linux

#endif // STREAM9_LINUX_PROCESS_WAIT_HPP

#ifndef STREAM9_LINUX_PROCESS_NAMESPACE_HPP
#define STREAM9_LINUX_PROCESS_NAMESPACE_HPP

namespace stream9::linux {

namespace lx { using namespace linux; }

} // namespace stream9::linux

#endif // STREAM9_LINUX_PROCESS_NAMESPACE_HPP

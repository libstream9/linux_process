#ifndef STREAM9_LINUX_PROCESS_PIPE_HPP
#define STREAM9_LINUX_PROCESS_PIPE_HPP

#include "namespace.hpp"

#include <fcntl.h> // flags constant

#include <stream9/linux/fd.hpp>
#include <stream9/linux/error.hpp>

namespace stream9::linux {

struct fd_pair {
    lx::fd read;
    lx::fd write;
};

fd_pair pipe();
fd_pair pipe(int flags);

} // namespace stream9::linux

#endif // STREAM9_LINUX_PROCESS_PIPE_HPP

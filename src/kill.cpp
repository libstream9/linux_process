#include <stream9/linux/process/kill.hpp>

#include "symbol.hpp"

namespace stream9::linux {

void
kill(::pid_t const pid, int const sig)
{
    auto const rc = ::kill(pid, sig);
    if (rc == -1) {
        throw error {
            linux::make_error_code(errno), {
                { "pid", pid },
                { "sig", signal_to_symbol(sig) },
            }
        };
    }
}

} // namespace stream9::linux

#include <stream9/linux/fork.hpp>

namespace stream9::linux {

pid
fork()
{
    try {
        auto id = ::fork();
        if (id == -1) {
            throw error {
                "fork()",
                lx::make_error_code(errno)
            };
        }
        else {
            return lx::pid(id);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux

#include <stream9/linux/setsid.hpp>

#include <stream9/linux/error.hpp>

namespace stream9::linux {

safe_integer<::pid_t, 0>
setsid()
{
    auto rc = ::setsid();
    if (rc == -1) {
        throw error {
            "setsid()",
            linux::make_error_code(errno)
        };
    }
    else {
        return rc;
    }
}

} // namespace stream9::linux

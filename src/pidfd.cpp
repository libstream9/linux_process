#include <stream9/linux/process/pidfd.hpp>

#include "symbol.hpp"

#include <sys/syscall.h>

namespace stream9::linux {

static int
make_pidfd(::pid_t const pid, unsigned int const flags)
{
    auto const rc = ::syscall(SYS_pidfd_open, pid, flags);
    if (rc == -1) {
        throw error {
            "pidfd_open()",
            linux::make_error_code(errno),
        };
    }

    return static_cast<int>(rc);
}

pidfd::
pidfd(::pid_t const pid, unsigned int const flags/*= 0*/)
    try : m_fd { make_pidfd(pid, flags) }
{}
catch (...) {
    rethrow_error({
        { "pid", pid },
        { "flags", flags },
    });
}

fd pidfd::
getfd(fd_ref const targetfd, unsigned int const flags/*= 0*/)
{
    auto const rc = ::syscall(SYS_pidfd_getfd, targetfd, flags);
    if (rc == -1) {
        throw error {
            "pidfd_gerfd()",
            linux::make_error_code(errno), {
                { "targetfd", targetfd },
                { "flags", flags },
            }
        };
    }

    return linux::fd { static_cast<int>(rc) };
}

void pidfd::
send_signal(int const sig, ::siginfo_t& info, unsigned int flags/*= 0*/)
{
    auto const rc = ::syscall(SYS_pidfd_send_signal, sig, &info, flags);
    if (rc == -1) {
        throw error {
            "pidfd_send_signal()",
            linux::make_error_code(errno), {
                { "sig", signal_to_symbol(sig) },
                { "flags", flags },
            }
        };
    }
}

void pidfd::
send_signal(int const sig, unsigned int flags/*= 0*/)
{
    auto const rc = ::syscall(SYS_pidfd_send_signal, sig, nullptr, flags);
    if (rc == -1) {
        throw error {
            "pidfd_send_signal()",
            linux::make_error_code(errno), {
                { "sig", signal_to_symbol(sig) },
                { "flags", flags },
            }
        };
    }
}

} // namespace stream9::linux

#include "symbol.hpp"

#include <signal.h>

#include <stream9/strings/stream.hpp>

namespace stream9::linux {

std::string
signal_to_symbol(int const sig)
{
    using strings::operator<<;
    std::string sym;

    switch (sig) {
        case SIGHUP: sym = "SIGHUP"; break;
        case SIGQUIT: sym = "SIGQUIT"; break;
        case SIGILL: sym = "SIGILL"; break;
        case SIGTRAP: sym = "SIGTRAP"; break;
        case SIGABRT: sym = "SIGABRT"; break;
        case SIGBUS: sym = "SIGBUS"; break;
        case SIGFPE: sym = "SIGFPE"; break;
        case SIGKILL: sym = "SIGKILL"; break;
        case SIGUSR1: sym = "SIGUSR1"; break;
        case SIGSEGV: sym = "SIGSEGV"; break;
        case SIGUSR2: sym = "SIGUSR2"; break;
        case SIGALRM: sym = "SIGALRM"; break;
        case SIGTERM: sym = "SIGTERM"; break;
        case SIGSTKFLT: sym = "SIGSTKFLT"; break;
        case SIGCHLD: sym = "SIGCHLD"; break;
        case SIGCONT: sym = "SIGCONT"; break;
        case SIGSTOP: sym = "SIGSTOP"; break;
        case SIGTSTP: sym = "SIGTSTP"; break;
        case SIGTTIN: sym = "SIGTTIN"; break;
        case SIGTTOU: sym = "SIGTTOU"; break;
        case SIGURG: sym = "SIGURG"; break;
        case SIGXCPU: sym = "SIGXCPU"; break;
        case SIGXFSZ: sym = "SIGXFSZ"; break;
        case SIGVTALRM: sym = "SIGVTALRM"; break;
        case SIGPROF: sym = "SIGPROF"; break;
        case SIGWINCH: sym = "SIGWINCH"; break;
        case SIGIO: sym = "SIGIO"; break;
        case SIGPWR: sym = "SIGPWR"; break;
        case SIGSYS: sym = "SIGSYS"; break;
        default:
            sym << "unknown (" << sig << ")";
            break;
    }

    return sym;
}

} // namespace stream9::linux

#include <stream9/linux/process/exec.hpp>

#include <unistd.h>

#include <stream9/json.hpp>

namespace stream9::linux {

static json::object
make_context(char const* path, args const& argv)
{
    json::object obj;

    obj["path"] = path;
    obj["args"] = argv;

    return obj;
}

static json::object
make_context(char const* path,
             args const& argv,
             args const& envp)
{
    auto obj = make_context(path, argv);
    obj["envp"] = envp;

    return obj;
}

void
exec(cstring_ptr filename, args const& argv)
{
    auto const argv_ = const_cast<char* const*>(argv.data());

    auto const rc = ::execv(filename, argv_);
    assert(rc == -1); (void)rc;

    throw error {
        "exec()",
        linux::make_error_code(errno),
        make_context(filename, argv),
    };
}

void
exec(cstring_ptr const filename,
     args const& argv,
     args const& envp)
{
    auto const argv_ = const_cast<char* const*>(argv.data());
    auto const envp_ = const_cast<char* const*>(envp.data());

    auto const rc = ::execve(filename, argv_, envp_);
    assert(rc == -1); (void)rc;

    throw error {
        "exec()",
        linux::make_error_code(errno),
        make_context(filename, argv, envp),
    };
}

void
execp(cstring_ptr const file,
      args const& argv)
{
    auto const argv_ = const_cast<char* const*>(argv.data());

    auto const rc = ::execvp(file, argv_);
    assert(rc == -1); (void)rc;

    throw error {
        "exec()",
        linux::make_error_code(errno),
        make_context(file, argv),
    };
}

void
execp(cstring_ptr const file,
      args const& argv,
      args const& envp)
{
    auto const argv_ = const_cast<char* const*>(argv.data());
    auto const envp_ = const_cast<char* const*>(envp.data());

    auto const rc = ::execvpe(file, argv_, envp_);
    assert(rc == -1); (void)rc;

    throw error {
        "exec()",
        linux::make_error_code(errno),
        make_context(file, argv, envp),
    };
}

namespace nothrow {

outcome<void, errc>
exec(cstring_ptr filename, args const& argv)
{
    auto const argv_ = const_cast<char* const*>(argv.data());

    auto const rc = ::execv(filename, argv_);
    assert(rc == -1);

    return static_cast<errc>(errno);
}

outcome<void, errc>
exec(cstring_ptr filename, args const& argv, args const& envp)
{
    auto const argv_ = const_cast<char* const*>(argv.data());
    auto const envp_ = const_cast<char* const*>(envp.data());

    auto const rc = ::execve(filename, argv_, envp_);
    assert(rc == -1);

    return static_cast<errc>(errno);
}

outcome<void, errc>
execp(cstring_ptr filename, args const& argv)
{
    auto const argv_ = const_cast<char* const*>(argv.data());

    auto const rc = ::execvp(filename, argv_);
    assert(rc == -1);

    return static_cast<errc>(errno);
}

outcome<void, errc>
execp(cstring_ptr filename, args const& argv, args const& envp)
{
    auto const argv_ = const_cast<char* const*>(argv.data());
    auto const envp_ = const_cast<char* const*>(envp.data());

    auto const rc = ::execvpe(filename, argv_, envp_);
    assert(rc == -1);

    return static_cast<errc>(errno);
}

} // namespace nothrow

} // namespace stream9::linux

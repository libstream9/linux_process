#ifndef STREAM9_LINUX_PROCESS_SYMBOL_HPP
#define STREAM9_LINUX_PROCESS_SYMBOL_HPP

#include <string>

namespace stream9::linux {

std::string signal_to_symbol(int sig);

} // namespace stream9::linux

#endif // STREAM9_LINUX_PROCESS_SYMBOL_HPP

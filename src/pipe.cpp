#include <stream9/linux/process/pipe.hpp>

#include <unistd.h>

#include <stream9/bits.hpp>

namespace stream9::linux {

static std::string
flags_to_symbol(int flags)
{
    return bits::ored_flags_to_symbol(flags, {
        STREAM9_FLAG_ENTRY(O_CLOEXEC),
        STREAM9_FLAG_ENTRY(O_DIRECT),
        STREAM9_FLAG_ENTRY(O_NONBLOCK),
    });
}

fd_pair
pipe()
{
    int fds[2];

    auto const rc = ::pipe(fds);
    if (rc == -1) {
        throw error { "pipe()", lx::make_error_code(errno) };
    }

    return {
        lx::fd(fds[0]),
        lx::fd(fds[1])
    };
}

fd_pair
pipe(int const flags)
{
    int fds[2];

    auto const rc = ::pipe2(fds, flags);
    if (rc == -1) {
        throw error { "pipe()", lx::make_error_code(errno), {
            { "flags", flags_to_symbol(flags) }
        } };
    }

    return {
        lx::fd(fds[0]),
        lx::fd(fds[1])
    };
}

} // namespace stream9::linux

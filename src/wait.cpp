#include <stream9/linux/process/wait.hpp>

#include <stream9/bits.hpp>
#include <stream9/json.hpp>

namespace stream9::linux {

static std::string
options_to_symbol(int const options)
{
    return bits::ored_flags_to_symbol(options, {
        STREAM9_FLAG_ENTRY(WNOHANG),
        STREAM9_FLAG_ENTRY(WUNTRACED),
        STREAM9_FLAG_ENTRY(WCONTINUED),
    });
}

safe_integer<::pid_t, 0>
wait(int* const wstatus/*= nullptr*/)
{
    safe_integer<::pid_t, -1> const rc = ::wait(wstatus);
    if (rc == -1) {
        throw error {
            "wait()",
            linux::make_error_code(errno)
        };
    }

    return rc;
}

safe_integer<::pid_t, 0>
waitpid(::pid_t const pid, int* const wstatus/*= nullptr*/)
{
    safe_integer<::pid_t, -1> const rc = ::waitpid(pid, wstatus, 0);
    if (rc == -1) {
        throw error {
            "waitpid()",
            linux::make_error_code(errno)
        };
    }

    return rc;
}

safe_integer<::pid_t, 0>
waitpid(::pid_t const pid, int* const wstatus, int const options)
{
    safe_integer<::pid_t, -1> const rc = ::waitpid(pid, wstatus, options);
    if (rc == -1) {
        throw error {
            "waitpid()",
            linux::make_error_code(errno), {
                { "options", options_to_symbol(options) }
            }
        };
    }

    return rc;
}

} // namespace stream9::linux

#include <stream9/linux/pid.hpp>

#include <sys/wait.h>

namespace stream9::linux {

pid::
pid(::pid_t id)
    : m_id { id }
{
    if (m_id < 0) {
        throw error {
            st9::errc::argument_out_of_domain, {
                { "pid", id }
            }
        };
    }
}

pid::
~pid() noexcept
{
    try {
        if (m_id > 0) {
            auto rc = ::waitpid(m_id, nullptr, 0);
            if (rc == -1) {
                if (errno == ECHILD) return;
                else {
                    throw error {
                        "waitpid(2)",
                        lx::make_error_code(errno), {
                            { "pid", m_id }
                        }
                    };
                }
            }
        }
    }
    catch (...) {
        print_error();
    }
}

::pid_t pid::
release() noexcept
{
    auto const r = m_id;
    m_id = -1;
    return r;
}

} // namespace stream9::linux
